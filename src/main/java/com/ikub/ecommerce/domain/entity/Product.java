package com.ikub.ecommerce.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "product_findByProductByCategory",
        query = "select p from Product p where p.category.id = : catId")
})
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "products")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Product {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String description;
    @ManyToOne
    @JoinColumn(name = "category_id",referencedColumnName = "id")
    private Category category;
    private Double price;
    @OneToMany(mappedBy = "product",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ProductOption> productOptions;
    @OneToMany(mappedBy = "product",fetch = FetchType.LAZY)
    private List<Review> reviews;
    private boolean enabled;



}
