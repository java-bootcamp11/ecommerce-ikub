package com.ikub.ecommerce.domain.entity;

import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum OrderStatus {

    CANCELLED("CANCELLED"),
    CONFIRMED("CONFIRMED"),
    IN_PROGRESS("IN_PROGRESS"),
    DELIVERED("DELIVERED");

    private String value;

    public static OrderStatus fromValue(String orderStatus){
        return Arrays.asList(OrderStatus.values())
                .stream().filter(r -> r.value.equals(orderStatus))
                .findFirst()
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Order Status %s not found",orderStatus)));
    }

    public String getValue() {
        return value;
    }
}
