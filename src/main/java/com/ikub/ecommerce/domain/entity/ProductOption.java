package com.ikub.ecommerce.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "product_options")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductOption {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String value;
    @ManyToOne
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private Product product;
    private Integer stock;
}
