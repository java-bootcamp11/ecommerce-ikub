package com.ikub.ecommerce.domain.entity;

import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum ShippingStatus {

    PENDING("PENDING"),
    REJECTED("REJECTED"),
    IN_TRANSIT("IN_TRANSIT"),
    READY("READY"),
    ARRIVED("ARRIVED");

    private String value;

    public static ShippingStatus fromValue(String value){
        return Arrays.asList(ShippingStatus.values())
                .stream().filter(r -> r.value.equals(value))
                .findFirst()
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Shipping Status %s not found",value)));
    }

    public String getValue() {
        return value;
    }
}
