package com.ikub.ecommerce.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "shipping")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Shipping {

    @Id
    @GeneratedValue
    private Integer id;
    private String shippingAdress;
    @Enumerated(EnumType.STRING)
    private ShippingStatus shippingStatus;
    private LocalDateTime shippingDate;
    private String shippingProvider;
}
