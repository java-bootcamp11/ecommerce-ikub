package com.ikub.ecommerce.domain.entity;

import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum ShippingProvider {

    UPS("UPS",10.0,2),
    DHL("DHL",15.0,2),
    ADEX("ADEX",2.0,3),
    IN_STORE("IN_STORE",0.0,0);

    private String value;
    private Double fee;
    private Integer delivery;

    public static ShippingProvider fromValue(String value){
        return Arrays.asList(ShippingProvider.values())
                .stream().filter(r -> r.value.equals(value))
                .findFirst()
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Shipping Provider %s not found",value)));
    }

    public String getValue() {
        return value;
    }

    public Double getFee() {
        return fee;
    }

    public Integer getDelivery() {
        return delivery;
    }
}
