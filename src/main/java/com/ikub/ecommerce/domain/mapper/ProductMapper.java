package com.ikub.ecommerce.domain.mapper;

import com.ikub.ecommerce.domain.dto.product.ProductCategoryDTO;
import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.dto.product.ProductOptionDTO;
import com.ikub.ecommerce.domain.dto.product.UpdateProductDTO;
import com.ikub.ecommerce.domain.entity.Category;
import com.ikub.ecommerce.domain.entity.Product;
import com.ikub.ecommerce.domain.entity.ProductOption;

import java.util.stream.Collectors;

public class ProductMapper {

    public static ProductDTO toDto(Product p){
        return ProductDTO.builder()
                .id(p.getId())
                .name(p.getName())
                .description(p.getDescription())
                .categoryDTO(p.getCategory()!=null?toDto(p.getCategory()):null)
                .price(p.getPrice())
                .optionDTOS(p.getProductOptions()!=null?p.getProductOptions().stream()
                        .map(o-> toDto(o)).collect(Collectors.toList()) : null)
                .enabled(p.isEnabled())
                .build();
    }

    public static ProductCategoryDTO toDto(Category c){
        return ProductCategoryDTO.builder()
                .id(c.getId())
                .name(c.getName())
                .parntCategory(c.getParent()!=null?ProductMapper.toDto(c.getParent()):null)
                .build();
    }

    public static Category toEntity(ProductCategoryDTO c){
        return Category.builder()
                .id(c.getId())
                .name(c.getName())
                .build();
    }

    public static ProductOptionDTO toDto(ProductOption o){
        return ProductOptionDTO.builder()
                .id(o.getId())
                .name(o.getName())
                .value(o.getValue())
                .stock(o.getStock())
                .build();
    }

    public static ProductOption toEntity(Product p,ProductOptionDTO dto){
        return ProductOption.builder()
                .id(dto.getId())
                .product(p)
                .name(dto.getName())
                .value(dto.getValue())
                .stock(dto.getStock())
                .build();
    }

    public static Product toEntityUpdate(Product p,UpdateProductDTO up){
        p.setName(up.getName());
        p.setDescription(up.getDescription());
        p.setPrice(up.getPrice());
        return p;
    }

    public static Product toEntityProd(ProductDTO up,Category cat){
        return Product.builder()
                .name(up.getName())
                .description(up.getDescription())
                .price(up.getPrice())
                .enabled(up.getEnabled())
                .category(cat)
                .build();
    }

}
