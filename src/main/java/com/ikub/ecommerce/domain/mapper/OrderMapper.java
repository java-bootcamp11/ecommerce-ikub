package com.ikub.ecommerce.domain.mapper;

import com.ikub.ecommerce.domain.dto.order.OrderDTO;
import com.ikub.ecommerce.domain.dto.order.OrderItemDto;
import com.ikub.ecommerce.domain.entity.*;

import java.util.List;
import java.util.stream.Collectors;

public class OrderMapper {

    public static OrderDTO toDto(Order o){
        return OrderDTO.builder()
                .id(o.getId())
                .customerEmail(o.getCustomerEmail())
                .customerName(o.getCustomerName())
                .orderStatus(o.getOrderStatus().getValue())
                .totalAmount(o.getTotalAmount())
                .orderItems(o.getOrderItems()!=null?o.getOrderItems().stream()
                        .map(OrderMapper::toDto).collect(Collectors.toList()) : null)
                .build();
    }

    public static OrderItemDto toDto(OrderItem item){
        return OrderItemDto.builder()
                .id(item.getId())
                .price(item.getPrice())
                .quantity(item.getQuantity())
                .productDTO(item.getProduct()!=null?ProductMapper.toDto(item.getProduct()):null)
                .optionDTO(item.getProductOption()!=null?ProductMapper.toDto(item.getProductOption()):null)
                .build();
    }

    public static Order buildOrder(User u,Order o){
        o.setCustomerName(u.getName());
        o.setCustomerEmail(u.getEmail());
        List<OrderItem> items = u.getCart().getCartItems().stream().map(item -> {
            OrderItem i = new OrderItem();
            i.setPrice(getPrice(item));
            i.setQuantity(item.getQuantity());
            i.setProduct(item.getProduct());
            i.setProductOption(item.getProductOption());
            i.setOrder(o);
            return i;
        } ).collect(Collectors.toList());
        o.setOrderItems(items);
        return o;
    }

    private static Double getPrice(CartItem item){
        if(item.getProduct()!=null)
            return item.getProduct().getPrice();
        else
            return item.getProductOption().getProduct().getPrice();
    }
}
