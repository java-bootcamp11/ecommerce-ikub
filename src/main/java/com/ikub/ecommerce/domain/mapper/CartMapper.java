package com.ikub.ecommerce.domain.mapper;

import com.ikub.ecommerce.domain.dto.cart.CartDTO;
import com.ikub.ecommerce.domain.dto.cart.CartItemDTO;
import com.ikub.ecommerce.domain.entity.Cart;
import com.ikub.ecommerce.domain.entity.CartItem;

import java.util.stream.Collectors;

public class CartMapper {

    public static CartItemDTO toDto(CartItem item){
        return CartItemDTO.builder()
                .id(item.getId())
                .quantity(item.getQuantity())
                .productDTO(item.getProduct()!=null?ProductMapper.toDto(item.getProduct()):null)
                .optionDTO(item.getProductOption()!=null?ProductMapper.toDto(item.getProductOption()):null)
                .build();
    }

    public static CartDTO toDto(Cart cart){
        return CartDTO.builder()
                .id(cart.getId())
                .items(cart.getCartItems().stream().map(i -> toDto(i)).collect(Collectors.toList()))
                .totalAmount(cart.getCartItems().stream()
                        .map(i-> (i.getProduct().getPrice() * i.getQuantity()))
                        .mapToDouble(Double::doubleValue).sum())
                .build();
    }
}
