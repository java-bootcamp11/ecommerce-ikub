package com.ikub.ecommerce.domain.exception;

public class ResourceNotFountException extends RuntimeException{

    public ResourceNotFountException(String msg){
        super(msg);
    }
}
