package com.ikub.ecommerce.domain.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductOptionDTO {

    private Integer id;
    private String name;
    private String value;
    private Integer stock;
}
