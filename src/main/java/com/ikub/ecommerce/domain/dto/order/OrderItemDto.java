package com.ikub.ecommerce.domain.dto.order;

import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.dto.product.ProductOptionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderItemDto {

    private Integer id;
    private Integer quantity;
    private Double price;
    private ProductDTO productDTO;
    private ProductOptionDTO optionDTO;
}
