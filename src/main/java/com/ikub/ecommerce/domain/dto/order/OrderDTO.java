package com.ikub.ecommerce.domain.dto.order;

import com.ikub.ecommerce.domain.entity.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderDTO {

    private Integer id;
    private String customerName;
    private String customerEmail;
    private List<OrderItemDto> orderItems;
    private Double totalAmount;
    private String orderStatus;
}
