package com.ikub.ecommerce.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CheckoutDTO {
    private String address;
    private String provider;
    private String paymentMethod;
}
