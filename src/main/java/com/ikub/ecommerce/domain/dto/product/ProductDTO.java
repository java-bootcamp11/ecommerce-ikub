package com.ikub.ecommerce.domain.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDTO {

    private Integer id;
    private String name;
    private String description;
    private ProductCategoryDTO categoryDTO;
    private Double price;
    private List<ProductOptionDTO> optionDTOS;
    private Boolean enabled;
}
