package com.ikub.ecommerce.domain.dto.cart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItemRequest {

    private Integer id;
    private Integer quantity;
    private Integer productId;
    private Integer productOptionId;
}
