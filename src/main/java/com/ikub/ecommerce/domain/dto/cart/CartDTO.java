package com.ikub.ecommerce.domain.dto.cart;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO {
    private Integer id;
    private List<CartItemDTO> items;
    private Double totalAmount;
}
