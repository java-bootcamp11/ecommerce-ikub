package com.ikub.ecommerce.domain.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductCategoryDTO {

    private Integer id;
    private String name;
    private ProductCategoryDTO parntCategory;
}
