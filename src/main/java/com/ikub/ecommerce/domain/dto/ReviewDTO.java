package com.ikub.ecommerce.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class ReviewDTO {
    private Integer userId;
    private Integer rating;
    private String comment;
}
