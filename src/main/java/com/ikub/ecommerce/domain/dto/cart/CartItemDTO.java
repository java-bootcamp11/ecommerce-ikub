package com.ikub.ecommerce.domain.dto.cart;

import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.dto.product.ProductOptionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartItemDTO {

    private Integer id;
    private Integer quantity;
    private ProductDTO productDTO;
    private ProductOptionDTO optionDTO;
}
