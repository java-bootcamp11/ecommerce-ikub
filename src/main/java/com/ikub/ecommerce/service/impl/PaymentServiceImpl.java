package com.ikub.ecommerce.service.impl;

import com.ikub.ecommerce.domain.entity.Payment;
import com.ikub.ecommerce.domain.entity.PaymentMethod;
import com.ikub.ecommerce.repository.PaymentRepository;
import com.ikub.ecommerce.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;

    @Override
    public Payment addPayment(String method, Double amount) {
        Payment p = new Payment();
        p.setPaymentMethod(PaymentMethod.fromValue(method));
        p.setAmount(amount);
        p.setTransactionId(UUID.randomUUID().toString());
        return paymentRepository.save(p);
    }

    @Override
    public Void refund(Integer paymentId) {
        paymentRepository.deleteById(paymentId);
        return null;
    }
}
