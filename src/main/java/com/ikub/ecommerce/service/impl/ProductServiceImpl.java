package com.ikub.ecommerce.service.impl;

import com.ikub.ecommerce.domain.dto.product.ProductCategoryDTO;
import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.dto.product.ProductOptionDTO;
import com.ikub.ecommerce.domain.dto.product.UpdateProductDTO;
import com.ikub.ecommerce.domain.entity.Category;
import com.ikub.ecommerce.domain.entity.Product;
import com.ikub.ecommerce.domain.entity.ProductOption;
import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import com.ikub.ecommerce.domain.mapper.ProductMapper;
import com.ikub.ecommerce.repository.ProductCategoryRepository;
import com.ikub.ecommerce.repository.ProductOptionRepository;
import com.ikub.ecommerce.repository.ProductRepository;
import com.ikub.ecommerce.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductOptionRepository optionRepository;
    private final ProductCategoryRepository categoryRepository;

    @Override
    public Product findById(Integer id) {
        return productRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Product with %s id not found",id)));
    }

    @Override
    public List<ProductDTO> findAll() {
        return productRepository.findAll()
                .stream()
                .map(ProductMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO addProduct(Integer catId,ProductDTO req) {
        Category category = categoryRepository.findById(catId)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Category with id %s not found",catId)));
        return ProductMapper.toDto(productRepository
                .save(ProductMapper.toEntityProd(req,category)));
    }

    @Override
    public ProductDTO updateProduct(Integer id, UpdateProductDTO req) {
        Product toUpdate = productRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Product with id %s not found",id)));
        return ProductMapper.toDto(productRepository
                .save(ProductMapper.toEntityUpdate(toUpdate,req)));
    }

    @Override
    public Boolean setProductStatus(Integer id) {
        Product product = productRepository.findById(id)
                .map(p-> {
                    p.setEnabled(p.isEnabled()?false:true);
                    return p;
                }).map(productRepository::save)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Product with id %s not found",id)));
        return product.isEnabled();
    }

    @Override
    public List<ProductDTO> getRelatedProducts(Integer id) {
        return productRepository.findById(id)
                .map(p ->
                        productRepository
                        .findByCategoryIdCustom(p.getCategory().getId()))
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Product with id %s not found",id)))
                .stream()
                .filter(p -> p.getId()!=id)
                .map(ProductMapper::toDto)
                .limit(5)
                .collect(Collectors.toList());

    }

    @Override
    public List<ProductDTO> findProductsByCategory(Integer categoryId) {
        return productRepository.findByCategoryIdCustom(categoryId)
                .stream().map(ProductMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductOptionDTO> getProductOptions(Integer productId) {
        return findById(productId).getProductOptions()
                .stream().map(ProductMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public ProductOptionDTO getProductOptionById(Integer optionId) {
        return optionRepository.findById(optionId)
                .map(ProductMapper::toDto)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("product option with id %s not found",optionId)));
    }

    @Override
    public ProductOptionDTO addProductOption(Integer productId, ProductOptionDTO req) {
        Product p = findById(productId);
        ProductOption option = ProductMapper.toEntity(p,req);
        return ProductMapper.toDto(optionRepository.save(option));
    }

    @Override
    public ProductOptionDTO updateProductOption(Integer productId, Integer optionId, ProductOptionDTO req) {
        req.setId(optionId);
        Product p = findById(productId);
        ProductOption op = p.getProductOptions().stream()
                .filter( o -> o.getId()== optionId)
                .findFirst()
                .map(o -> ProductMapper.toEntity(p,req))
                .orElseThrow(()->new ResourceNotFountException(String
                        .format("Product option with id %s not found",optionId)));
        return ProductMapper.toDto(optionRepository.save(op));
    }

//    @Override
//    public Void deleteOptionById(Integer optionId) {
//        optionRepository.findById(optionId)
//                .ifPresentOrElse(o -> optionRepository.deleteById(optionId),
//                        ()-> new ResourceNotFountException(String
//                                .format("product option with id %s not found",optionId)));
//        return null;
//    }

    @Override
    public Void deleteOptionById(Integer optionId) {
        optionRepository.findById(optionId)
                .orElseThrow(()-> new ResourceNotFountException(String
                                .format("product option with id %s not found",optionId)));
        optionRepository.deleteById(optionId);
        return null;
    }

    @Override
    public ProductCategoryDTO addCategory(ProductCategoryDTO req) {
        Category toSave = ProductMapper.toEntity(req);
        if(req.getParntCategory()!=null){
            toSave.setParent(categoryRepository.findById(req.getParntCategory().getId())
                    .orElseThrow(()-> new ResourceNotFountException(String
                            .format("parent category with id %s not found",req.getParntCategory().getId()))));
        }
        return ProductMapper.toDto(categoryRepository.save(toSave));
    }

    @Override
    public List<ProductCategoryDTO> getCategories() {
        return categoryRepository.findAll().stream()
                .map(ProductMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public ProductCategoryDTO getCategoriesById(Integer id) {
        return categoryRepository.findById(id)
                .map(ProductMapper::toDto)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("category with id %s not found",id)));
    }

    @Override
    public Void deleteCategory(Integer id) {
        Category cat = categoryRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("category with id %s not found",id)));
        categoryRepository.delete(cat);
        return null;
    }
}
