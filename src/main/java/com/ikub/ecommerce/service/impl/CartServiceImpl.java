package com.ikub.ecommerce.service.impl;

import com.ikub.ecommerce.domain.dto.cart.CartDTO;
import com.ikub.ecommerce.domain.dto.cart.CartItemRequest;
import com.ikub.ecommerce.domain.entity.Cart;
import com.ikub.ecommerce.domain.entity.CartItem;
import com.ikub.ecommerce.domain.entity.ProductOption;
import com.ikub.ecommerce.domain.entity.User;
import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import com.ikub.ecommerce.domain.mapper.CartMapper;
import com.ikub.ecommerce.repository.*;
import com.ikub.ecommerce.service.CartService;
import com.ikub.ecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import static com.ikub.ecommerce.configuration.SecurityConfig.getJwt;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final UserService userService;
    private final CartItemRepository cartItemRepository;
    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final ProductOptionRepository optionRepository;


    @Override
    public CartDTO getCart() {
        return CartMapper.toDto(userService.getUserFromToken(getJwt()).getCart());
    }

    @Override
    public CartDTO addCartItem(CartItemRequest item) {
        User user = userService.getUserFromToken(getJwt());
        System.err.println("user "+user.getEmail());
        Cart userCart = user.getCart();
        System.err.println("cart size is "+ (userCart==null));
        CartItem itemToAdd;
        if(userCart==null){
            userCart = new Cart();
            userCart.setUser(user);
            userCart = cartRepository.save(userCart);
            itemToAdd = buildItem(userCart,item);
        }else {
            itemToAdd = buildItem(userCart,item);
        }
        userCart.getCartItems().add(itemToAdd);
        Cart cart = cartRepository.save(userCart);
        return CartMapper.toDto(cartRepository.findById(userCart.getId()).get());
    }

    @Override
    public CartDTO removeItem(Integer itemId) {
        User user = userService.getUserFromToken(getJwt());
        cartItemRepository.deleteById(itemId);
        return CartMapper.toDto(cartRepository
                .findById(user.getCart().getId()).get());
    }

    private CartItem buildItem(Cart cart, CartItemRequest req){
        CartItem item = new CartItem();
        item.setCart(cart);
        item.setQuantity(req.getQuantity());

        if(req.getProductId()!=null){
            item.setProduct(productRepository.findById(req.getProductId())
            .orElseThrow(()-> new ResourceNotFountException(String
                    .format("Product with id %s not found",req.getProductId()))));
        }else if(req.getProductOptionId()!=null){
            ProductOption op = optionRepository.findById(req.getProductOptionId())
                    .orElseThrow(()-> new ResourceNotFountException(String
                            .format("Product with id %s not found",req.getProductId())));
//            item.setProduct(op.getProduct());
            item.setProductOption(op);
        }
        return item;
    }
}
