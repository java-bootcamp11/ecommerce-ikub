package com.ikub.ecommerce.service.impl;

import com.ikub.ecommerce.domain.dto.CheckoutDTO;
import com.ikub.ecommerce.domain.entity.Shipping;
import com.ikub.ecommerce.domain.entity.ShippingProvider;
import com.ikub.ecommerce.domain.entity.ShippingStatus;
import com.ikub.ecommerce.repository.ShippingRepository;
import com.ikub.ecommerce.service.ShippingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class ShippingServiceImpl implements ShippingService {

    private final ShippingRepository shippingRepository;

    @Override
    public Shipping addShipping(CheckoutDTO req) {
        ShippingProvider provider = ShippingProvider.fromValue(req.getProvider());
        Shipping shipping = new Shipping();
        shipping.setShippingDate(LocalDateTime.now().plusDays(provider.getDelivery()));
        shipping.setShippingProvider(provider.getValue());
        shipping.setShippingAdress(req.getAddress());
        shipping.setShippingStatus(ShippingStatus.PENDING);
        return shippingRepository.save(shipping);

    }

    @Override
    public Void setShippingStatus(Integer shippingId, String status) {
         shippingRepository.findById(shippingId)
                .map(sh -> {
                    sh.setShippingStatus(ShippingStatus.fromValue(status));
                    return  shippingRepository.save(sh);
                });
        return null;
    }
}
