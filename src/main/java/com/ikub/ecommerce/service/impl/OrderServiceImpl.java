package com.ikub.ecommerce.service.impl;

import com.ikub.ecommerce.domain.dto.CheckoutDTO;
import com.ikub.ecommerce.domain.dto.order.OrderDTO;
import com.ikub.ecommerce.domain.entity.*;
import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import com.ikub.ecommerce.domain.mapper.OrderMapper;
import com.ikub.ecommerce.repository.OrderRepository;
import com.ikub.ecommerce.service.OrderService;
import com.ikub.ecommerce.service.PaymentService;
import com.ikub.ecommerce.service.ShippingService;
import com.ikub.ecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ShippingService shippingService;
    private final PaymentService paymentService;
    private final UserService userService;


    @Override
    public List<OrderDTO> getOrders() {
        return orderRepository.findAll().stream()
                .map(OrderMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<OrderDTO> getOrdersByCustomerId(String customerEmail) {
        return orderRepository.findAllByCustomerEmail(customerEmail)
                .stream().map(OrderMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public OrderDTO processOrder(Jwt jwt, CheckoutDTO checkout) {
        User u = userService.getUserFromToken(jwt);
        Order o = orderRepository.save(new Order());
        o = OrderMapper.buildOrder(u,o);
        o.setTotalAmount(o.getOrderItems().stream().map(i->i.getPrice())
                .mapToDouble(Double::doubleValue).sum());
        o.setOrderStatus(OrderStatus.IN_PROGRESS);

        o = orderRepository.save(o);

        Shipping shipping = shippingService.addShipping(checkout);
        Payment payment = paymentService.addPayment(checkout.getPaymentMethod(),o.getTotalAmount());

        o.setShipping(shipping);
        o.setPayment(payment);
        o.setOrderStatus(OrderStatus.CONFIRMED);
        o = orderRepository.save(o);
        return OrderMapper.toDto(o);
    }

    @Override
    public Void setOrderStatus(Integer orderId,String status) {
        orderRepository.findById(orderId)
                .map(o-> {
                    o.setOrderStatus(OrderStatus.fromValue(status));
                    return orderRepository.save(o);
                }).orElseThrow(()->new ResourceNotFountException("order not found"));
        return null;
    }
}
