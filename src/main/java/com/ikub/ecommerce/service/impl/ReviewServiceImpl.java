package com.ikub.ecommerce.service.impl;

import com.ikub.ecommerce.domain.dto.ReviewDTO;
import com.ikub.ecommerce.domain.entity.Product;
import com.ikub.ecommerce.domain.entity.Review;
import com.ikub.ecommerce.domain.entity.User;
import com.ikub.ecommerce.domain.exception.ResourceNotFountException;
import com.ikub.ecommerce.repository.ProductRepository;
import com.ikub.ecommerce.repository.ReviewRepository;
import com.ikub.ecommerce.repository.UserRepository;
import com.ikub.ecommerce.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository repository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Override
    public Void addReview(Integer productId, ReviewDTO reviewDTO) {
        Product product = productRepository.findById(productId)
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("Product with id %s not found",productId)));
        User user = userRepository.findById(reviewDTO.getUserId())
                .orElseThrow(()-> new ResourceNotFountException(String
                        .format("User with id %s not found",reviewDTO.getUserId())));
        Review review = new Review();
        review.setRating(reviewDTO.getRating());
        review.setComment(reviewDTO.getComment());
        review.setProduct(product);
        review.setUser(user);
        repository.save(review);
        return null;
    }

    @Override
    public Void deleteReview(Integer reviewId) {
        repository.deleteById(reviewId);
        return null;
    }
}
