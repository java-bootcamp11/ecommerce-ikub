package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.dto.cart.CartDTO;
import com.ikub.ecommerce.domain.dto.cart.CartItemDTO;
import com.ikub.ecommerce.domain.dto.cart.CartItemRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;

public interface CartService {
    CartDTO getCart();
    CartDTO addCartItem(CartItemRequest item);
    CartDTO removeItem(Integer itemId);
}
