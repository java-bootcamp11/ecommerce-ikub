package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.entity.Payment;

public interface PaymentService {

    Payment addPayment(String method,Double amount);
    Void refund(Integer paymentId);
}
