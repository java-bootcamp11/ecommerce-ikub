package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.dto.CheckoutDTO;
import com.ikub.ecommerce.domain.dto.order.OrderDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.List;

public interface OrderService {
    List<OrderDTO> getOrders();
    List<OrderDTO>  getOrdersByCustomerId(String custId);
    OrderDTO processOrder(Jwt jwt,CheckoutDTO checkout);
    Void setOrderStatus(Integer orderId,String status);
}
