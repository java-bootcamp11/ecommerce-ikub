package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.dto.product.ProductCategoryDTO;
import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.dto.product.ProductOptionDTO;
import com.ikub.ecommerce.domain.dto.product.UpdateProductDTO;
import com.ikub.ecommerce.domain.entity.Category;
import com.ikub.ecommerce.domain.entity.Product;

import java.util.List;

public interface ProductService {
    Product findById(Integer id);
    List<ProductDTO> findAll();
    ProductDTO addProduct(Integer catId,ProductDTO req);
    ProductDTO updateProduct(Integer id,UpdateProductDTO req);
    Boolean setProductStatus(Integer id);
    List<ProductDTO> getRelatedProducts(Integer id);
    List<ProductDTO> findProductsByCategory(Integer categoryId);
    List<ProductOptionDTO> getProductOptions(Integer productId);
    ProductOptionDTO getProductOptionById(Integer optionId);
    ProductOptionDTO addProductOption(Integer productId, ProductOptionDTO req);
    ProductOptionDTO updateProductOption(Integer productId,Integer optionId,ProductOptionDTO req);
    Void deleteOptionById(Integer optionId);
    ProductCategoryDTO addCategory(ProductCategoryDTO req);
    List<ProductCategoryDTO> getCategories();
    ProductCategoryDTO getCategoriesById(Integer id);
    Void deleteCategory(Integer id);
}
