package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.dto.user.UserDTO;
import com.ikub.ecommerce.domain.dto.user.UserUpdateDTO;
import com.ikub.ecommerce.domain.entity.User;
import org.springframework.security.oauth2.jwt.Jwt;

public interface UserService {
    User findById(Integer id);
    UserDTO registerUser(UserDTO req, String userRole);
    UserUpdateDTO updateUser(Integer id,UserUpdateDTO req);
    User getUserFromToken(Jwt jwt);
}
