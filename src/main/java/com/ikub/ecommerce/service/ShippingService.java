package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.dto.CheckoutDTO;
import com.ikub.ecommerce.domain.entity.Shipping;

public interface ShippingService {
    Shipping addShipping(CheckoutDTO req);
    Void setShippingStatus(Integer shippingId,String status);
}
