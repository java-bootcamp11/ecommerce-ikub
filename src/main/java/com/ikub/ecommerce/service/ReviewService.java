package com.ikub.ecommerce.service;

import com.ikub.ecommerce.domain.dto.ReviewDTO;

public interface ReviewService {

    Void addReview(Integer productId,ReviewDTO reviewDTO);
    Void deleteReview(Integer reviewId);
}
