package com.ikub.ecommerce.controller;

import com.ikub.ecommerce.domain.dto.CheckoutDTO;
import com.ikub.ecommerce.domain.dto.cart.CartDTO;
import com.ikub.ecommerce.domain.dto.cart.CartItemRequest;
import com.ikub.ecommerce.domain.dto.order.OrderDTO;
import com.ikub.ecommerce.service.CartService;
import com.ikub.ecommerce.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/orders")
@RequiredArgsConstructor
public class OrderController {

    private final CartService cartService;
    private final OrderService orderService;

    @GetMapping("/cart")
    public ResponseEntity<CartDTO> getCart(){
        return ResponseEntity.ok(cartService.getCart());
    }

    @PostMapping("/cart")
    public ResponseEntity<CartDTO> addCartItem(@RequestBody CartItemRequest req){
        return ResponseEntity.ok(cartService.addCartItem(req));
    }

    @DeleteMapping("/cart/{itemId}")
    public ResponseEntity<CartDTO> addCartItem(@PathVariable Integer itemId){
        return ResponseEntity.ok(cartService.removeItem(itemId));
    }

    @PostMapping
    public ResponseEntity<OrderDTO> createOrder(@AuthenticationPrincipal Jwt jwt, @RequestBody CheckoutDTO ch){
        return ResponseEntity.ok(orderService.processOrder(jwt,ch));
    }

    @GetMapping("/{custEmail}")
    public ResponseEntity<List<OrderDTO>> getOrdersByCustomerEmail(@PathVariable String custEmail){
        return ResponseEntity.ok(orderService.getOrdersByCustomerId(custEmail));
    }

    @GetMapping("/{orderId}/{orderStatus}")
    public ResponseEntity<Void> setOrderStatus(@PathVariable Integer orderId,@PathVariable String orderStatus){
        return ResponseEntity.ok(orderService.setOrderStatus(orderId,orderStatus));
    }
}
