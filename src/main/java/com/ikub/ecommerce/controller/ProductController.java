package com.ikub.ecommerce.controller;

import com.ikub.ecommerce.domain.dto.ReviewDTO;
import com.ikub.ecommerce.domain.dto.product.ProductCategoryDTO;
import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.dto.product.ProductOptionDTO;
import com.ikub.ecommerce.domain.dto.product.UpdateProductDTO;
import com.ikub.ecommerce.domain.mapper.ProductMapper;
import com.ikub.ecommerce.service.ProductService;
import com.ikub.ecommerce.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RequiredArgsConstructor
@RestControllerAdvice
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;
    private final ReviewService reviewService;

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> findById(@PathVariable Integer id){
        ProductDTO p = ProductMapper.toDto(productService.findById(id));
        return ResponseEntity.ok(p);
    }

    @GetMapping("/list")
    public ResponseEntity<List<ProductDTO>> findAllProducts(){
        return ResponseEntity.ok(productService.findAll());
    }


    @RolesAllowed("ADMIN")
    @PostMapping("/admin/{categoryId}")
    public ResponseEntity<ProductDTO> addProduct(@PathVariable Integer categoryId,
                                                 @RequestBody ProductDTO req ){
        return ResponseEntity.ok(productService.addProduct(categoryId,req));
    }

    @RolesAllowed("ADMIN")
    @PutMapping("/admin/{id}")
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable Integer id,@RequestBody UpdateProductDTO req ){
        return ResponseEntity.ok(productService.updateProduct(id,req));
    }

    @RolesAllowed("ADMIN")
    @GetMapping("/admin/{id}/status")
    public ResponseEntity<Boolean> setProductStatus(@PathVariable Integer id){
        return ResponseEntity.ok(productService.setProductStatus(id));
    }

    @GetMapping("/{id}/related")
    public ResponseEntity<List<ProductDTO>> getRelatedProducts(@PathVariable Integer id){
        return ResponseEntity.ok(productService.getRelatedProducts(id));
    }

    @GetMapping("/category/{categoryId}/list")
    public ResponseEntity<List<ProductDTO>> getProductsByCategory(@PathVariable Integer categoryId){
        return ResponseEntity.ok(productService.findProductsByCategory(categoryId));
    }

    @PostMapping("/{productId}/review")
    public ResponseEntity<Void> addReview(@PathVariable Integer productId,@RequestBody ReviewDTO req){
        return ResponseEntity.ok(reviewService.addReview(productId,req));
    }


    @GetMapping("{productId}/options")
    public ResponseEntity<List<ProductOptionDTO>> getProductOptions(@PathVariable Integer productId ){
        return ResponseEntity.ok(productService.getProductOptions(productId));
    }

    @GetMapping("/options/{optionId}")
    public ResponseEntity<ProductOptionDTO> getProductOptionById(@PathVariable Integer optionId ){
        return ResponseEntity.ok(productService.getProductOptionById(optionId));
    }

    @PostMapping("/{productId}/admin/options")
    public ResponseEntity<ProductOptionDTO> addOption(@PathVariable Integer productId,@RequestBody ProductOptionDTO req ){
        return ResponseEntity.ok(productService.addProductOption(productId,req));
    }

    @RolesAllowed("ADMIN")
    @PutMapping("/{productId}/admin/options/{optionId}")
    public ResponseEntity<ProductOptionDTO> updateProductOption(@PathVariable Integer productId, @PathVariable Integer optionId,
                                                                @RequestBody ProductOptionDTO req){
        return ResponseEntity.ok(productService.updateProductOption(productId,optionId,req));
    }

    @RolesAllowed("ADMIN")
    @DeleteMapping("/admin/options/{optionId}")
    public ResponseEntity<Void> deleteOptionById(@PathVariable Integer optionId){
        return ResponseEntity.ok(productService.deleteOptionById(optionId));
    }

    @RolesAllowed("ADMIN")
    @PostMapping("/admin/categories")
    public ResponseEntity<ProductCategoryDTO> addCategory(@RequestBody ProductCategoryDTO category){
        return ResponseEntity.ok(productService.addCategory(category));
    }

    @GetMapping("/list/categories")
    public ResponseEntity<List<ProductCategoryDTO>> getCategories(){
        return ResponseEntity.ok(productService.getCategories());
    }

    @GetMapping("/categories/{catId}")
    public ResponseEntity<ProductCategoryDTO> getCategories(@PathVariable Integer catId){
        return ResponseEntity.ok(productService.getCategoriesById(catId));
    }

    @RolesAllowed("ADMIN")
    @DeleteMapping("/admin/categories/{catId}")
    public ResponseEntity<Void> deleteCategories(@PathVariable Integer catId){
        return ResponseEntity.ok(productService.deleteCategory(catId));
    }
}