package com.ikub.ecommerce.repository;

import com.ikub.ecommerce.domain.entity.Category;
import com.ikub.ecommerce.domain.entity.Product;
import com.ikub.ecommerce.repository.custom.ProductCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer>, ProductCustomRepository {

    List<Product> findAllByCategory_Id(Integer cateory);

    @Query(name = "product_findByProductByCategory")
    List<Product> findByCategoryIdCustom(Integer catId);
}
