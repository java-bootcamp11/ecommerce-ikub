package com.ikub.ecommerce.repository.custom;

import com.ikub.ecommerce.domain.entity.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ProductCustomRepositoryImpl implements ProductCustomRepository{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Product> findProductsByCategoryId(Integer categoryId) {
        return entityManager
                .createNamedQuery("product_findByProductByCategory",Product.class)
                .setParameter("catId",categoryId)
                .getResultList();
    }
}
