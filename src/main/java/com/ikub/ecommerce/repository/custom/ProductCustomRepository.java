package com.ikub.ecommerce.repository.custom;

import com.ikub.ecommerce.domain.entity.Product;

import java.util.List;

public interface ProductCustomRepository {
    List<Product> findProductsByCategoryId(Integer categoryId);
}
