package com.ikub.ecommerce.repository;

import com.ikub.ecommerce.domain.entity.ProductOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductOptionRepository extends JpaRepository<ProductOption,Integer> {
}
