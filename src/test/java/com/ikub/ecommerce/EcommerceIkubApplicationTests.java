package com.ikub.ecommerce;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static  org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EcommerceIkubApplicationTests {

	@Autowired
	EcommerceIkubApplication toTest;

	@Test
	@Disabled
	void test_findIndex_ok() {
		assertAll(
				()-> assertEquals(2,toTest.findIndex(new int[]{1,2,3,4,5},3))
		);

	}

}
