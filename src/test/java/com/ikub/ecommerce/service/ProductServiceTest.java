package com.ikub.ecommerce.service;

import com.ikub.ecommerce.BaseTest;
import com.ikub.ecommerce.domain.dto.product.ProductCategoryDTO;
import com.ikub.ecommerce.domain.dto.product.ProductDTO;
import com.ikub.ecommerce.domain.entity.Category;
import com.ikub.ecommerce.domain.entity.Product;
import com.ikub.ecommerce.domain.mapper.ProductMapper;
import com.ikub.ecommerce.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.parameters.P;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProductServiceTest extends BaseTest {

    @Autowired
    private ProductService toTest;

    @MockBean
    private ProductRepository productRepository;

    @Test
    public void test_getRelatedProducts_ok(){
        Product fakeProduct = new Product();
        fakeProduct.setCategory(new Category());

        Mockito.doReturn(Optional.of(fakeProduct))
                .when(productRepository).findById(Mockito.any());

        List<Product> fakeProducts = new ArrayList<>();
        Product p = new Product();
        p.setId(1);
        Product p1 = new Product();
        p1.setId(2);
        fakeProducts.add(p1);
        fakeProducts.add(p);

        Mockito.doReturn(fakeProducts).when(productRepository)
                .findByCategoryIdCustom(Mockito.any());

        List<ProductDTO> out = toTest.getRelatedProducts(1);
        Assertions.assertEquals(1,out.size());


    }
}
